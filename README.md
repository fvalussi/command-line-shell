# Command Line Shell

## Usage

```shell
yarn install

yarn build

node ./lib/index.js
```

## Available commands

- pwd
- ls
- mkdir
- touch
- cd
