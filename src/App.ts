import { CommandProcessor } from "./CommandProcessor";
import * as readline from "readline";
import { Directory } from "./Directory";

export class App {
  private commandProcessor;
  private rl;

  constructor() {
    this.commandProcessor = new CommandProcessor(Directory.createRoot());
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
  }

  private processInput = (input: string) => {
    if (input == "quit") {
      this.rl.close();
      return;
    }
    const output = this.commandProcessor.process(input);
    this.rl.write(output);
    this.rl.write("\n");
    this.rl.question("", this.processInput);
  };

  launch() {
    this.rl.question("", this.processInput);
  }
}
