import { Directory } from "./Directory";
import { OnCommand } from "./OnCommand";
import { OnPwdCommand } from "./OnPwdCommand";
import { OnLsCommand } from "./OnLsCommand";
import { OnMkdirCommand } from "./OnMkdirCommand";
import { OnCdCommand } from "./OnCdCommand";
import { OnTouchCommand } from "./OnTouchCommand";
import { OnUnrecognizedCommand } from "./OnUnrecognizedCommand";

export class CommandProcessor {
  private onCommand: OnCommand = new OnUnrecognizedCommand();
  private updateCurrentDirectory = (directory: Directory) => {
    this.currentDirectory = directory;
  };

  constructor(private currentDirectory: Directory) {}

  process = (commandWithArguments: string) => {
    const { command, arg } = this.splitCommandAndArguments(
      commandWithArguments
    );

    switch (command) {
      case "pwd":
        this.onCommand = new OnPwdCommand(this.currentDirectory);
        break;
      case "ls":
        this.onCommand = new OnLsCommand(this.currentDirectory);
        break;
      case "mkdir":
        this.onCommand = new OnMkdirCommand(this.currentDirectory, arg);
        break;
      case "cd":
        this.onCommand = new OnCdCommand(
          this.currentDirectory,
          arg,
          this.updateCurrentDirectory
        );
        break;
      case "touch":
        this.onCommand = new OnTouchCommand(this.currentDirectory, arg);
        break;
      default:
        this.onCommand = new OnUnrecognizedCommand();
    }

    return this.onCommand.execute();
  };

  private splitCommandAndArguments = (commandWithArguments: string) => {
    const [command, arg] = commandWithArguments.split(" ");
    return { command, arg };
  };
}
