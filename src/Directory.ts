import { MyFile } from "./MyFile";

export class Directory {
  constructor(
    private name: string,
    private directories: Directory[] = [],
    private path = "/",
    private parent: Directory | null = null,
    private files: MyFile[] = []
  ) {}

  static createRoot(): Directory {
    return new Directory("root");
  }

  static create(name: string) {
    if (name.length > 100) throw new Error("Directory name exceeds max length");
    return new Directory(name);
  }

  listDirectories(): string {
    let directories = "";
    this.directories.forEach((d) => {
      directories += `${d.name}\n`;
    });
    return directories;
  }

  listFiles() {
    let files = "";
    this.files.forEach((f: MyFile) => {
      files += `${f.getName()}\n`;
    });
    return files;
  }

  addDirectory(directory: Directory) {
    const existingDirectory = this.directories.find((d: Directory) =>
      d.isNamed(directory.getName())
    );
    if (!!existingDirectory) throw new Error("Directory already exists");
    directory.path = this.path + this.name + "/";
    directory.parent = this;
    this.directories.push(directory);
  }

  addFile(file: MyFile) {
    const existingFile = this.files.find((f: MyFile) =>
      f.isNamed(file.getName())
    );
    if (!!existingFile) throw new Error("File already exists");
    this.files.push(file);
  }

  getFullPath() {
    return this.path + this.name;
  }

  getName() {
    return this.name;
  }

  getDirectory(directoryName: string) {
    const directory = this.directories.find((d: Directory) =>
      d.isNamed(directoryName)
    );
    if (!directory) throw new Error("Directory not found");
    return directory;
  }

  getParent(): Directory {
    if (!this.parent) return this;
    return this.parent;
  }

  private isNamed(name: string) {
    return this.name === name;
  }
}
