export class MyFile {
  constructor(private name: string) {}

  static create(fileName: string) {
    if (fileName.length > 100) throw new Error("File name exceeds max length");
    return new MyFile(fileName);
  }

  getName() {
    return this.name;
  }

  isNamed(name: string) {
    return this.name === name;
  }
}
