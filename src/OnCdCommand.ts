import { OnCommand } from "./OnCommand";
import { Directory } from "./Directory";

export class OnCdCommand implements OnCommand {
  constructor(
    private currentDirectory: Directory,
    private arg: string,
    private updateCurrentDirectory: (directory: Directory) => void
  ) {}

  execute(): string {
    const directoryName = this.arg;
    if (directoryName === "..") {
      this.updateCurrentDirectory(this.currentDirectory.getParent());
      return "";
    }
    try {
      this.updateCurrentDirectory(
        this.currentDirectory.getDirectory(directoryName)
      );
      return "";
    } catch (e) {
      return e.message;
    }
  }
}
