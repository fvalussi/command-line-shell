export interface OnCommand {
  execute(): string;
}
