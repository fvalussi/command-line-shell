import { OnCommand } from "./OnCommand";
import { Directory } from "./Directory";

export class OnLsCommand implements OnCommand {
  constructor(private currentDirectory: Directory) {}

  execute(): string {
    const directories = this.currentDirectory.listDirectories();
    const files = this.currentDirectory.listFiles();
    return directories + files;
  }
}
