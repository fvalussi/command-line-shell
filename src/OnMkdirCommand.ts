import { OnCommand } from "./OnCommand";
import { Directory } from "./Directory";

export class OnMkdirCommand implements OnCommand {
  private readonly directoryName: string;

  constructor(private currentDirectory: Directory, arg: string) {
    this.directoryName = arg;
  }

  execute(): string {
    try {
      const newDirectory = Directory.create(this.directoryName);
      this.currentDirectory.addDirectory(newDirectory);
    } catch (e) {
      return e.message;
    }

    return "";
  }
}
