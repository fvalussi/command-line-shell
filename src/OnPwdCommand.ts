import { OnCommand } from "./OnCommand";
import { Directory } from "./Directory";

export class OnPwdCommand implements OnCommand {
  constructor(private currentDirectory: Directory) {}

  execute = () => this.currentDirectory.getFullPath();
}
