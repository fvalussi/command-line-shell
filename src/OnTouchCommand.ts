import { OnCommand } from "./OnCommand";
import { Directory } from "./Directory";
import { MyFile } from "./MyFile";

export class OnTouchCommand implements OnCommand {
  private readonly fileName: string;

  constructor(private currentDirectory: Directory, arg: string) {
    this.fileName = arg;
  }

  execute(): string {
    try {
      const newFile = MyFile.create(this.fileName);
      this.currentDirectory.addFile(newFile);
      return "";
    } catch (e) {
      return e.message;
    }
  }
}
