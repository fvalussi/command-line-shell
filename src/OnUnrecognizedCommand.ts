import { OnCommand } from "./OnCommand";

export class OnUnrecognizedCommand implements OnCommand {
  execute(): string {
    return "Unrecognized command";
  }
}
