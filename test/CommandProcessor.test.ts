import { CommandProcessor } from "../src/CommandProcessor";
import { Directory } from "../src/Directory";

describe("CommandProcessor", function () {
  test("test 01", () => {
    const commandProcessor = new CommandProcessor(Directory.createRoot());

    const output = commandProcessor.process("");

    expect(output).toBe("Unrecognized command");
  });

  test("test 02", () => {
    const commandProcessor = new CommandProcessor(Directory.createRoot());

    const output = commandProcessor.process("pwd");

    expect(output).toBe("/root");
  });

  test("test 03", () => {
    const commandProcessor = new CommandProcessor(Directory.createRoot());

    const output = commandProcessor.process("ls");

    expect(output).toBe("");
  });

  test("test 04", () => {
    let rootDirectory = Directory.createRoot();
    rootDirectory.addDirectory(Directory.create("abc"));
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process("ls");

    expect(output).toBe("abc\n");
  });

  test("test 05", () => {
    let rootDirectory = Directory.createRoot();
    rootDirectory.addDirectory(Directory.create("abc"));
    rootDirectory.addDirectory(Directory.create("def"));
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process("ls");

    expect(output).toBe("abc\n" + "def\n");
  });

  test("test 06", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("mkdir abc");
    const output = commandProcessor.process("ls");

    expect(output).toBe("abc\n");
  });

  test("test 07", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("mkdir abc");
    const output = commandProcessor.process("mkdir abc");

    expect(output).toBe("Directory already exists");
  });

  test("test 07b", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process(`mkdir ${getLongName()}`);

    expect(output).toBe("Directory name exceeds max length");
  });

  test("test 08", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("mkdir abc");
    commandProcessor.process("cd abc");
    const output = commandProcessor.process("pwd");

    expect(output).toBe("/root/abc");
  });

  test("test 09", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process("cd abc");

    expect(output).toBe("Directory not found");
  });

  test("test 10", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("mkdir abc");
    commandProcessor.process("cd abc");
    commandProcessor.process("cd ..");
    const output = commandProcessor.process("pwd");

    expect(output).toBe("/root");
  });

  test("test 11", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("cd ..");
    const output = commandProcessor.process("pwd");

    expect(output).toBe("/root");
  });

  test("test 12", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("touch newFile");
    const output = commandProcessor.process("ls");

    expect(output).toBe("newFile\n");
  });

  test("test 13", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("mkdir abc");
    const output = commandProcessor.process("cd abc");

    expect(output).toBe("");
  });

  test("test 13", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process("touch newFile");

    expect(output).toBe("");
  });

  test("test 14", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    const output = commandProcessor.process(`touch ${getLongName()}`);

    expect(output).toBe("File name exceeds max length");
  });

  test("test 15", () => {
    let rootDirectory = Directory.createRoot();
    const commandProcessor = new CommandProcessor(rootDirectory);

    commandProcessor.process("touch newFile");
    const output = commandProcessor.process("touch newFile");

    expect(output).toBe("File already exists");
  });

  function getLongName() {
    let name = "";
    for (let i = 0; i <= 101; i++) name += "a";
    return name;
  }
});
